((nil . ((cider-preferred-build-tool . shadow-cljs)
	 (cider-default-cljs-repl . shadow)
	 (cider-shadow-default-options . ":ashaw")
	 (cider-offer-to-open-cljs-app-in-browser . nil)
	 (cider-repl-pop-to-buffer-on-connect . nil))))
