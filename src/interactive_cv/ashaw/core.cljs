(ns interactive-cv.ashaw.core
  (:require [interactive-cv.core :as cv]
            [interactive-cv.demo.core :as demo]
            [interactive-cv.route :as route]
            interactive-cv.ashaw.component
            interactive-cv.element
            [interactive-cv.org-edn :as org]
            [garden.core :refer [css]]
            [garden.selectors :as gss]
            [garden.stylesheet :refer [at-font-face]]
            [shadow.resource :refer [inline]]))

(inline "content/ashaw/content.org")

(defn forward []
  [:div
   (when-let [t @cv/next-route]
     [:div "→ " [:a {:href t} (route/slug->str t)]])])

(defn root-component []
  [:div {:style {:background-color (:content-background @cv/org-header)
                 :opacity          (if (:once @cv/state) 1 0)
                 :height           "100vh"
                 :transition (str "opacity " (:transition-time @cv/context))}}

   [:div#header {:style {:background-color (:header-color @cv/context)
                         :transition (str "background-color "
                                          (:transition-time @cv/context))
                         :height "10%"
                         :padding "1rem"}}
    [:div {:style {:width "100%"}}
     [:style
      (css [(gss/descendant :div#header :a)
            {:color (:header-foreground @cv/context)
             :transition (str "color " (:transition-time @cv/context))}])]
     (when (not= "false" (get @cv/context :show-navigation? "true"))
       [demo/top-nav {:style {:font-family (:body-font @cv/context)}}])]]
   [:div {:style {:display          "flex"
                  :background-color (:content-background @cv/context)
                  :justify-content  "center"
                  :min-height       "80%"
                  :font-family      (:body-font @cv/context)}}

    [:style (css (at-font-face {:font-family "calendas_plusregular"
                                :src "url(\"font/calendas_plus-webfont.ttf\")"}))]
    [:div {:style {:width            "800px"
                   :margin-bottom    "4rem"
                   :box-shadow       (str (:shadow-x @cv/context) "rem "
                                          (:shadow-y @cv/context) "rem "
                                          "0.2rem rgba(0,0,0,0.5)")
                   :max-height       "60%"
                   :background-color (:content-background @cv/context)
                   :transition       (str "background-color " (:transition-time @cv/context)
                                          ", "
                                          "box-shadow " (:transition-time @cv/context))
                   :display          "flex"
                   :padding-top      "4rem"
                   :flex-direction   "column"}}
     (some-> @cv/page org/zipper org/render)]]])

(defn init []
  (cv/fetch-content
   {:on-response #(set! js/document.title (:title @cv/org-header))})
  (cv/mount root-component))
