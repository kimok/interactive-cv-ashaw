(ns interactive-cv.ashaw.component
  (:require [clojure.string :as str]
            [clojure.zip :as z]
            [interactive-cv.core :as cv]
            [interactive-cv.zip :as cv.zip]
            [interactive-cv.route :refer [route]]
            [interactive-cv.component :as c]
            [interactive-cv.org-edn :as org
             :refer [component]]
            [gnu.org-mode :as-alias org-mode]
            [garden.core :refer [css]]
            [garden.units :refer [percent]]
            [garden.stylesheet :refer [at-keyframes]]))

(defmethod component ::c/COLORTITLE [node]
  (-> (org/loc node)
      (z/edit org/with-type :div)
      z/down
      (z/edit org/with-type ::c/COLORTITLE.title)
      z/up))

(defmethod component ::c/COLORTITLE.title [_]
  (let [root-route? (= @route @cv/root-route)
        colorwords
        (into [:h1
               {:style
                {:font-size (when-let [size (:title-size @cv/context)] (str size "px"))
                 :position "relative"
                 :text-align (when root-route? "center")
                 :top (if (:once @cv/state) 0 -50)
                 :transition (str "top " (:transition-time @cv/context))
                 :font-weight "normal"
                 :margin-bottom "4rem"}}]
              (map #(do [:a {:href @cv/root-route
                             :style {:color %1}} %2])
                   (str/split (:palette @cv/context) #" ")
                   (str/split (:title @cv/context) #" ")))]
    (cond-> colorwords
      root-route?
      (vary-meta assoc :drop-children true)
      (not root-route?)
      (into [" :: "]))))

(defmethod component ::c/LINKLIST [node]
  (-> (:loc (meta node))
      (z/edit org/with-type :div)
      (org/seek-descendant ::org-mode/list)
      (z/edit org/with-type ::LINKLIST.links)))

(defmethod component ::LINKLIST.links [node]
  (into  ^:drop-children ^:dbg

   [:div {:style {:display "flex"}}]
         (->> (org/children node)
              (map #(org/with-type % ::LINKLIST.item)))))

(defmethod component ::LINKLIST.item [node]
  (let [loc (:loc (meta node))
        tag (org/some-node loc ::org-mode/tag)
        link (org/some-node loc ::org-mode/link)
        image (org/some-node loc ::org-mode/link.file)]
    ^:drop-children ^:dbg
    [:a {:href (org/prop link :path)
         :style {:max-width "20%"
                 :margin-top (* 200 (rand))
                 :transition "margin-top 10s"
                 :min-height 200
                 :display "flex"
                 :flex-direction "column"
                 :align-content "center"}} image
     (org/with-type tag :div)]))

(defmethod component ::c/SIDEIMAGE [node]
  (let [loc (org/loc node)
        cover (org/some-node loc ::org-mode/link.file)
        content (some-> loc
                        (z/edit org/update-props assoc ::origin true)
                        (org/seek-descendant ::org-mode/link.file)
                        z/remove
                        (cv.zip/unseek (comp ::origin org/props))
                        (z/edit org/update-props dissoc ::origin)
                        z/children)]
    ^:drop-children
    [:div {:style {:display "flex"
                   :width "100%"
                   :justify-content "center"
                   :flex-wrap "wrap"
                   :flex-direction "row"
                   :margin-bottom "4rem"}}
     [:div {:style {:max-width "90%"
                    :display "flex"}}
      [:div {:style {:max-width "30%"}} cover]
      [:div {:style {:flex 1
                     :margin-left "2rem"}}
       (into [:div {:style {:text-align "center"
                            :justify-content "center"
                            :width "100%"}}]
             content)]]]))

(defmethod component ::c/FADECAROUSEL [node]
  (let [children (->> node org/children rest (filter org/org-element?))
        step (int (/ (:seconds @cv/state) 4))
        steps (range (count children))
        place-state (get-in @cv/state [node :place])
        running? (not (some? place-state))
        place (or place-state (nth (cycle steps) step) 0)
        bullet (fn [i]
                 [:a {:href "#"
                      :on-click
                      #(doto cv/state
                         (swap! update node assoc :place i)
                         (swap! assoc (nth children i) false))
                      :style {:display "block"}}
                  (if (#{i} place) "●" "○")])]
    ^:drop-children
    [:div {:style {:display "flex"
                   :width "100%"}}
     [:style (css (at-keyframes "fade-carousel"
                                [(percent 0) {:opacity 0}]
                                [(percent 10) {:opacity 1}]
                                [(percent 80) {:opacity 1}]
                                (when running?
                                  [(percent 100) {:opacity 0}])))]
     (into [:div] (map bullet) steps)
     [:div {:style {:width "100%"}
            :on-click #(swap! cv/state update node assoc :place place)}
      [:div {:key place
             :style {:animation "fade-carousel 4s linear"
                     :animation-fill-mode "forwards"
                     :width "100%"
                     :height "100%"}}
       (nth (cycle children) place)]]]))

(defmethod component ::c/EXPANDER [node]
  (let [loc (org/loc node)
        cover (org/some-node loc ::c/EXPANDERCOVER)
        content (org/some-node loc ::c/EXPANDERBODY)]
    ^:drop-children
    [:div {:style {:display "flex"
                   :width "100%"
                   :justify-content "center"
                   :flex-wrap "wrap"
                   :flex-direction "row"
                   :margin-bottom "4rem"}}
     (if-not (get @cv/state node)
       [:div {:style {:max-width "90%"
                      :display "flex"
                      :text-align "center"
                      :justify-content "center"}}
        [:div {:on-click #(swap! cv/state update node not)
               :style {:flex 0.5
                       :cursor :pointer}}
         cover]]
       [:div {:style {:flex 1
                      :margin "0 5rem 0 5rem"}}
        [:div {:style {:text-align "center"
                             :justify-content "center"
                       :width "100%"}}
         content]])]))

(defmethod component ::c/EXPANDERCOVER [node] [:<>])
(defmethod component ::c/EXPANDERBODY [node] [:<>])

(defmethod component ::c/CARD [node]
  (let [loc (org/loc node)
        tag (-> loc
                (org/some-node ::org-mode/link)
                (org/with-type :a))
        image (-> loc
                  (org/some-node ::org-mode/link.file))

        path (org/prop tag :path)]
    ^:drop-children
    [:a {:href path
         :style {:display "flex"
                 :justify-content "center"}}
     [:div {:style {:position "relative"
                    :border-radius "20px"}}
      image
      [:div {:style {:position "absolute"}} tag]]]))

(defmethod component ::c/BIGCARD [node]
  (let [loc (org/loc node)
        tag (-> loc
                (org/some-node ::org-mode/link)
                (org/with-type :a))
        image (-> loc
                  (org/some-node ::org-mode/link.file))
        tag-path (org/prop tag :path)
        image-path (org/prop image :path)]
    ^:drop-children
    [:div {:href tag-path
           :style {:display "flex"
                   :justify-content "center"}}
     [:a {:href tag-path
          :style {:position "relative"
                  :background-size "cover"
                  :width "100%"
                  :margin "0 3rem 0 3rem"
                  :height "30rem"
                  :border-radius "20px"}}
      [:img {:margin "2rem"
             :src image-path
             :style {:width "100%"
                     :height "100%"
                     :min-height "20rem"
                     :object-fit "cover"}}]
      [:div {:style {:bottom "0%"
                     :left "35%"
                     :margin-left "1rem"
                     :position "absolute"}}
       (into [:h2 {:style {:color "white"
                           :text-stroke "5px black"
                           #_#_:text-shadow "2px 2px 2px black"}}] (org/children tag))]]]))
