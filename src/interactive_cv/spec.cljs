(ns interactive-cv.spec
  (:require [cljs.spec.alpha :as s]))

(s/def :interactive-cv.spec.section-todo/type
  #(= "todo" %))

(s/def :interactive-cv.spec.section-todo/keyword
  #{"SECTION"})

(s/def :interactive-cv.spec.section/type
  #{"section"})

(s/def :interactive-cv.spec.section.headline/type
  #{"headline"})

(s/def :interactive-cv.spec.section/headline
  (s/keys :req-un [:interactive-cv.spec.section.headline/type]))

(s/def :interactive-cv.spec.section.block/type
  #{"block"})

(s/def :interactive-cv.spec.section/block
  (s/keys :req-un [:interactive-cv.spec.section.block/type]))

(s/def :interactive-cv.spec.section.link/type
  #{"link"})

(s/def :interactive-cv.spec.section/link
  (s/keys :req-un [:interactive-cv.spec.section.link/type]))

(s/def :interactive-cv.spec/section-todo
  (s/keys :req-un [:interactive-cv.spec.section-todo/type
                     :interactive-cv.spec.section-todo/keyword]))

(s/def :interactive-cv.spec.section/children
  (fn [children]
    (some #(= "SECTION" (:keyword %)) children)))

(s/def :interactive-cv.spec/section
  (s/keys :req-un [:interactive-cv.spec.section/type
                   :interactive-cv.spec.section/children]))
