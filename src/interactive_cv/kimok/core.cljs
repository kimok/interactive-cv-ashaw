(ns interactive-cv.kimok.core
  (:require [interactive-cv.core :as cv]
            [interactive-cv.kimok.view :as view]
            [shadow.resource]))

(shadow.resource/inline "content/kimok/content.org")

(defn app []
  [:div {:style {:display "flex"
                 :height "100vh"
                 :background-color "white"}}
   [view/sidebar]
   [view/page]
   [view/contact]])

(defn init []
  (cv/fetch-content)
  (cv/mount app))
