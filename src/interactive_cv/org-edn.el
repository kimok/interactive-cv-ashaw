(require 'project)
(let ((lib (concat (project-root (project-current)) "lib/")))
  (add-to-list 'load-path (concat lib "parseclj/"))
  (load (concat lib "parseedn/parseedn.el")))

(require 'parseedn)

(defun str->el (el)
  (if (stringp el)
      (org-element-create :gnu.org-mode/text `(:value ,(substring-no-properties el)))
    el))

(defun k->sym (k)
  (intern (substring (symbol-name k) 1)))

(defun insert-secondary-values (el)
  (when (listp el)
    (let* ((type (org-element-type el))
           (keys (alist-get type org-element-secondary-value-alist)))
      (dolist (k keys)
        (let* ((prop (org-element-property k el))
               (type (k->sym k))
               (new-el `(,type
                         (:secondary-value?
                          ,t
                          :gnu.org-mode/element-type
                          ,type
                          :level
                          ,(org-element-property :level el)))))
          (when prop
            (apply #'org-element-set-contents
                 (cons el
                       (append (list (append new-el
                                             (if (listp prop) prop (list prop))))
                                     (org-element-contents el)))))))
      el)))

(defun insert-value-prop (el)
  (when (and (listp el)
             (eq 'special-block (car el)))
    (let* ((start (org-element-property :contents-begin el))
           (end   (org-element-property :contents-end el)))
      (when (and start end)
        (org-element-put-property el :value (buffer-substring start end))))))

(defun org->edn (output-filename)
  (org-mode)
  (setq org-link-types-re "\\`\\([a-z]+\\):"
        org-use-sub-superscripts nil)
  (let* ((tree (org-element-parse-buffer)))
    (org-element-put-property tree :gnu.org-mode/element-type 'org-data)
    (org-element-map tree (append org-element-all-elements
                                  org-element-all-objects '(plain-text))
      (lambda (el)
        (insert-secondary-values el)
        (insert-value-prop el)
        (org-element-put-property el :gnu.org-mode/element-type (org-element-type el))        
        (if (org-element-property :parent el)
            (org-element-put-property el :parent nil))
        (if (org-element-property :structure el)
            (org-element-put-property el :structure nil))))
    (with-temp-file output-filename
      (insert (parseedn-print-str tree)))))

