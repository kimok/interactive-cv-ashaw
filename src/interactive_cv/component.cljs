(ns interactive-cv.component
  (:require
   [clojure.zip :as z]
   [interactive-cv.core :as cv]
   [interactive-cv.org-edn :as org :refer [component]]
   [interactive-cv.doc :as doc :refer [docs]]
   [org.gnu.org-mode :as-alias org-mode]))

(let [todos (some-> @cv/org-header
                    :todo
                    (clojure.string/split #" "))
      keys (map (partial keyword (namespace ::_)) todos)]
  (doall (for [k keys] (derive k :interactive-cv/component))))

(derive :interactive-cv/component ::org-mode/headline)

(defmethod component ::EXAMPLE [_]
  [:div {:style {:display    "inline-block"
                 :padding "2rem"
                 :background (str "linear-gradient("
                                  "rgb(128, 255, 0), "
                                  "rgb(0, 255, 255))")}}])

(defmethod component ::EXAMPLE-CHILDREN [node]
  (into
   ^:drop-children
   [:div {:style {:display    "inline-block"
                  :padding "2rem"
                  :background (str "linear-gradient("
                                   "rgb(0, 128, 255), "
                                   "rgb(0, 255, 255))")}}]
   (drop 1)
   (org/children node)))

(defmethod component ::TODO [_]
  [:div [:strong {:style {:position "absolute"
                          :margin-top "-0.5rem"
                          :color "orange"
                          :text-shadow "1.5px 1px 0px black"
                          :transform "rotate(15deg)"}} "TODO"]])

(doc/add ::EXAMPLE "Example component. Adds a colorful background.

Usage:
** EXAMPLE <title>
<contents>")

(defmethod component ::DOC [node]
  (let [n (-> node (org/prop :title) first)
        doc (filter (comp #{n} name key)
                    @docs)]
    (into
     ^:drop-children
     [:pre {:style {:background "lightgray"
                    :max-width "50em"}}]
     (map (fn [e] [:div
                   [:h4 (str (key e))]
                   [:p (val e)]]))
     doc)))

(defmethod component ::SPACER [node]
  [:<> [:div {:style {:min-height "2rem"}}]])

(defmethod component ::MARGIN [_]
  [:div {:style {:margin "2rem"}}])

(defmethod component ::CENTER [_]
  [:div {:style {:text-align "center"}}])

(defmethod component ::HEADER [node]
    (-> node
      org/loc
      z/down
      (z/edit org/update-props assoc :level 1)))

(defmethod component ::HEADER1 [node]
  (-> node
      org/loc
      z/down
      (z/edit org/update-props assoc :level 1)))

(defmethod component ::HEADER2 [node]
  (-> node
      org/loc
      z/down
      (z/edit org/update-props assoc :level 2)))

(defmethod component ::HEADER3 [node]
  (-> node
      org/loc
      z/down
      (z/edit org/update-props assoc :level 3)))

(defmethod component ::HEADER4 [node]
  (-> node
      org/loc
      z/down
      (z/edit org/update-props assoc :level 4)))

(defmethod component ::HEADER5 [node]
  (-> node
      org/loc
      z/down
      (z/edit org/update-props assoc :level 5)))

(defmethod component ::HEADER6 [node]
  (-> node
      org/loc
      z/down
      (z/edit org/update-props assoc :level 6)))
