
* zip
:PROPERTIES:
:org-remark-file: ~/interactive-cv/src/interactive_cv/zip.cljc
:END:

** walk
:PROPERTIES:
:org-remark-beg: 200
:org-remark-end: 204
:org-remark-id: d78513fc
:org-remark-label: blue
:org-remark-link: [[file:~/interactive-cv/src/interactive_cv/zip.cljc::11]]
:END:
It would be interesting to optimize this, see if a reduce or transduce would be faster.
