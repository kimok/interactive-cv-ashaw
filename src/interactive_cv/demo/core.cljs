(ns interactive-cv.demo.core
  (:require [clojure.string :as str]
            interactive-cv.element
            interactive-cv.component
            [interactive-cv.route :as route :refer [route]]
            [interactive-cv.core :as cv]
            [interactive-cv.org-edn :as org]
            [shadow.resource]))

(shadow.resource/inline "content/demo/content.org")

(defn nav []
  (let [ts (map org/tag @cv/child-routes)]
    (when-not (empty? ts)
      (into [:div {:style {:display "flex"}} "↓"]
            (comp (map #(do [:a {:href %
                                 :style {:margin "0 0.5em 0 0.5em"}}
                             (route/slug->str %)]))
                  (interpose "|"))
            ts))))

(defn up []
  [:div
   (when-let [t (org/tag @cv/parent-route)]
     [:span [:a {:href t} "↑ " (route/slug->str t)]])])

(defn forward []
  [:div
   (when-let [t @cv/next-route]
     [:div [:a {:href t} "→ " (route/slug->str t)]])])

(defn back []
  [:div
   (when-let [t @cv/prev-route]
     [:div [:a {:href t} "← " (route/slug->str t)]])])

(defn top-nav [{:keys [style]}]
  [:div {:style (merge {:display "flex"
                        :justify-content "space-between"}
                       style)}
   [back] [up] [forward]])

(defn app []
  [:div {:style {:display "float"
                 :background "gray"
                 :padding "4rem"}}
   [:div {:style {:background "white"
                  :margin "auto"
                  :border-radius "1rem"
                  :max-width "800px"
                  :min-height "80vh"
                  :padding "4rem"}}
    [top-nav]
    (some-> @cv/page org/zipper org/render)
    [:br]
    [nav]]])

(defn init []
  (cv/fetch-content
   {:on-response #(when-not @route
                    (route/go! "interactive-cv"))})
  (cv/mount app))
