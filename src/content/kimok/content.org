#+author: Kimo Knowles
#+email: kimo@tuta.io
#+startup: nologdone

* SECTION me                                                             :me:
  I'm an artist, educator and engineer.

  <- Click a circle to start. Click away to start over.

** SECTION learn                                                      :learn:
  My knowledge is absolute, but I'm still learning to integrate it with the social order.

  The more I learn, the less successful this tends to be.
*** SECTION PSU                                                         :PSU:
  I attended to Portland State University from 2007 to 2009. I studied Computer Engineering with a minor in Jazz.
  I didn't finish any of that, because I ran out of funding and felt uninspired.
  I decided I should learn more about people and less about machines, which I did at SAIC.

  Now I know too much about people, which alienates me from them. And I still exploit machines for money. Oh well.

  Calculus was interesting, and boolean logic has been very useful. Jazz was traumatic.

  I worked in a Microscopy lab, using ion beams to slice crystallized beetle scales and do forensic analysis on industrial tethers
  .
  [[img/cemn1.jpg]]
*** SECTION SAIC                                                       :SAIC:
  From 2010 to 2014 I attended SAIC with the help of a merit scholarship for my music.

  SAIC is known for non-traditional approach to art education, with strong leanings toward postmodern conceptual art and critical theory.

  I took classes in:

  - audio & video projects
  - performance art
  - game design & programming
  - 3D industrial design
  - print media
  - philosophy, history, media archaeology

  I worked as a teaching assistant for classes in digital literacy and lighting design. In the Light lab, I mentored undergraduate and
graduate students with projects in neon, LED and mixed media. I taught wood & metal fabrication, electronics, neon bending and programming.

  #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/113980917 :width 640 :height 360
  #+END_EMBED

  [[img/lightlab1.jpg]]
  [[img/lightlab2.jpg]]
  [[img/lightlab3.jpg]]

*** SECTION independent                                         :independent:
  This is how most of my learning happened.

  Unbound from institutions, I have learned:

**** Philosophy
     Among the texts I've read, I consider each of these a subject of my lifelong study.
     - Steps to an Ecology of Mind :: Gregory Bateson
     - The Mass Psychology of Fascism :: Wilhelm Reich
     - The Conquest of Bread :: Peter Kropotkin
     - The Uses of the Erotic :: Audre Lorde
     - Marriage and Morals :: Bertrand Russell
     - The Trial :: Franz Kafka
     - The Plague :: Albert Camus
     - Genealogy of Morals :: Friedrich Nietzsche
     - The Structure of Magic :: Richard Bandler and John Grinder
**** 普通话
   Despite modern simplifications, Chinese is a dense language that restructured my mind. I learned by painting, SRS training, playing with children and riding taxis.
   [[img/shufa.jpg]]
**** Linux
   I installed Gentoo when I was sixteen, and never looked at Windows the same way again. I advocate free software whenever I can. I mostly use Debian at home and on web servers.

   I've taken deep dives into Docker, KVM, SELinux, OpenRC and custom kernels.
**** Health
   As a child I survived a life-threatening surgery which left me unable to stand up straight.

   After many years of ignoring it, I've rehabilitated my body through exercise and ergonomics.

   #+BEGIN_VIDEO youtube :url https://www.youtube.com/embed/0GiBO0n8mj8 :width 500 :height 300
   #+END_VIDEO

**** Cooking
   Inspired by Food Not Bombs and Hare Krishna free restaurants, I learned Indian cooking.

   It's the best way to cook, balancing complex and satisfying flavors, economy of scale, spiritual expression and medicinal nutrition.
   [[img/indian-food.jpg]]

** SECTION teach                                                      :teach:
  I have taught childen, adolescents and adults. Teaching is the best way to learn.
*** SECTION STEAM                                                     :steam:
  From 2017 to 2020, I taught STEAM for children and adolescents.
  - Robotics & Computer science
  - Curriculum design
  - Editing & Proofreading
  - Scratch, AppInventor, MBlock
  - PyMinecraft, Redstone
  - Html, Css, Javascript
  - Makeblock, M5Stack, DJI Tello
    [[img/egg-science.jpg]]
    [[img/construction-challenge.jpg]]
    [[img/robot-camp.jpg]]
    [[img/pi-camp.jpg]]
** SECTION build                                                      :build:
  I exploit technology for fun and profit. Here are some notable things I've made.
*** SECTION websites                                                    :web:
    The web is an interesting mess. I've created sites and services, using technologies such as:
    - React
    - Clojure/Clojurescript
    - PHP
    - Wordpress/Gutenberg
    - jQuery
**** SECTION truvoice                                              :truvoice:

I designed and built this new site for TruVoice. I extended a wordpress base with
[[https://youtu.be/riW5ycGETuk][custom blocks]], themes and integrations for Hubspot CRM And Acuity Scheduling. I'm currently working to replace the
jquery frontend with cljs/reagent/re-frame. This is part of a larger plan to create a web &
mobile app for teachers and clients to book lessons, assign practice and evaluate progress.

[[https://truvoicelessons.com][Visit Truvoice]]

#+BEGIN_VIDEO youtube :url https://www.youtube.com/embed/riW5ycGETuk :width 700 :height 400
#+END_VIDEO

**** SECTION kimo.life                                            :kimo-life:
  This interactive portfolio was my first [[https://reagent-project.github.io/cljs/reagent][cljs/reagent]] project.

  <- I call this the bubbler. I wrote it with geometry, pure functions and css.

  The content you're reading is drafted in [[https://orgmode.org/][org-mode]] and parsed as clojure data.
  I like Clojure! It combines the formal idealism of lisp with the practicality of java and js. If it's the last language I learn, I won't mind.

**** SECTION Kimo Knowles on Tumblr :tumblr:
   I wrote this minimal theme for my portfolio on tumblr. I also created an interactive
   background animation with Processing.org.
#+BEGIN_EMBED webpage :url https://kimoknowles.tumblr.com :width 100% :height 600
#+END_EMBED
**** SECTION Stan the Cobot                                            :stan:
   A simple, responsive landing page. Written in css on wordpress.
#+BEGIN_EMBED webpage :url https://stanthecobot.com :width 100% :height 600
#+END_EMBED
*** SECTION LED mapping :pixel-mapping:
  From 2013 to 2017, I developed this software for LED mapping.

  It integrates tools and frameworks such as:
  - Touchdesigner/Python
  - Rhino/Grasshopper
  - Ableton Live
  - Max/MSP
  - Arduino
#+BEGIN_VIDEO youtube :url https://www.youtube.com/embed/QvNpeMoDJLs :width 500 :height 300
#+END_VIDEO
*** SECTION Wearables                                              :wearables:
  From 2013 to 2017, I led a small team to prototype set pieces and wearables with realtime animated LED lighting.
  #+BEGIN_EMBED webpage :url https://trello.com/b/obRUuRKp.html :width 100% :height 600
  #+END_EMBED

** SECTION create                                                    :create:
  I don't have children, but I have these projects to inspire, defy me and break my heart.
*** SECTION scores                                                   :scores:
  I have arranged music for piano, guitar and big band. I use emacs, lilypond and R.

  In 2019 I collaborated with the author of [[https://github.com/leonawicz/tabr/issues/15][tabr]] to design a [[https://github.com/leonawicz/tabr/issues/15][new]] [[https://github.com/leonawicz/tabr/issues/39][syntax]] for transcribing guitar music in R.

  I hope to release my next project as a combination album and songbook. Here are some scores I've produced.

  [[pdf/zhongyao-pinyin.pdf]]
  [[pdf/zhongyao-clar.pdf]]
  [[pdf/centerstage.pdf]]

*** SECTION albums                                                   :albums:
  I have been writing, performing and producing music for a long time.
  Here are some of my favorites.

  #+BEGIN_EMBED soundcloud :url https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/301044413&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true :width 50% :height 300
  #+END_EMBED

  #+BEGIN_EMBED soundcloud :url https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1810191&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true :width 50% :height 300
  #+END_EMBED

  #+BEGIN_EMBED soundcloud :url https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/2517976&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true :width 50% :height 300
  #+END_EMBED
*** SECTION videos                                                   :videos:

**** Production
  I wrote, acted, created sets and did sound design for this short film.

  #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/20820330?title=0&byline=0&portrait=0&app_id=122963 :width 540 :height 360 :allowfullscreen true
  #+END_EMBED

  I did stop-motion, music and acting for this music video.

  #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/23570517?title=0&byline=0&portrait=0&app_id=122963 :width 640 :height 360 :allowfullscreen true
  #+END_EMBED

**** Post-production
  I did sound design, audio editing and restoration for this feature film.
  #+BEGIN_EMBED youtube :width 500 :height 281 :url https://www.youtube.com/embed/PWcETSQFxD8 :allowfullscreen true
  #+END_EMBED

  I did sound design and foley for this short animation.
     #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/27907348?title=0&byline=0&portrait=0&app_id=122963 :width 640 :height 360 :allowfullscreen true
     #+END_EMBED

  I did sound design for this video performance.
     #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/143496065 :width 640 :height 360 :allowfullscreen true
     #+END_EMBED

**** Experiments
   Here are some more strange things I've made...
   #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/106986929 :width 640 :height 360 :allowfullscreen true
     #+END_EMBED
   #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/38900121 :width 640 :height 360 :allowfullscreen true
     #+END_EMBED
   #+BEGIN_EMBED vimeo :url https://player.vimeo.com/video/56825962 :width 640 :height 360 :allowfullscreen true
     #+END_EMBED
